import object.Decorator;
import object.impl.Chocolade;
import object.impl.StarCoffee;

public class DecoratorRunner {

    public static void main(String[] args) {
        Decorator drink = new Chocolade(new Chocolade(new Chocolade(new StarCoffee())));
        System.out.println("Drink: " + drink.getDescription());
        System.out.println("Cost: " + drink.getCost());
    }
}
