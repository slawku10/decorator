package object;

public interface Drink {
    float getCost();
    String getDescription();
}
