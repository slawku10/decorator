package object.impl;

import object.Decorator;
import object.Drink;

public class Chocolade implements Decorator {
    private Drink drink;
    private float cost;
    private String description;

    public Chocolade(Drink drink) {
        this.drink = drink;
        this.cost = 0.2f;
        this.description = "Choocolade";
    }

    @Override
    public float getCost() {
        return drink.getCost() + this.cost;
    }

    @Override
    public String getDescription() {
        return drink.getDescription() + " with " + this.description;
    }
}
