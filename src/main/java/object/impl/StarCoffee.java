package object.impl;

import object.Drink;

public class StarCoffee implements Drink {
    private float cost;
    private String description;

    public StarCoffee() {
        this.cost = 1.0f;
        this.description = "This is special coffee drink";
    }

    @Override
    public float getCost() {
        return this.cost;
    }

    @Override
    public String getDescription() {
        return this.description;
    }
}
